//
//  CDSong+CoreDataProperties.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 21.07.2021.
//
//

import Foundation
import CoreData


extension CDSong {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDSong> {
        return NSFetchRequest<CDSong>(entityName: "CDSong")
    }

    @NSManaged public var artist: String?
    @NSManaged public var imageName: String?
    @NSManaged public var isFavorite: Bool
    @NSManaged public var length: String?
    @NSManaged public var title: String?

}

extension CDSong : Identifiable {

}
