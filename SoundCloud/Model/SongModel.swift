//
//  SongModel.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 28.06.2021.
//

import Foundation

struct Song {
    
    var title: String
    var artist: String
    var length: String
    var imageName: String
    var isFavorite: Bool
    
        static let songNames = [
                        "Mumiy Troll - Дельфины",
                        "ATB - The Summer",
                        "Pendulum, Liam Howlett - Immunize feat. Liam Howlett",
                        "Moby - Extreme Ways",
                        "Sub Focus - Solar System",
                        "Muffler - Start Line",
                        "John B, Shaz Sparks - Red Sky",
                        "Lindsey Stirling - Crystallize",
                        "Burial - Archangel",
                        "Drumsound & Bassline Smith - Memories"
                        ]
    
   static func getSongs() -> [Song] {
        var songs = [Song]()
        
        for song in songNames {
            songs.append(Song(title: song.components(separatedBy: " - ")[1], artist: song.components(separatedBy: " - ")[0], length: String(Int.random(in: 2...4)) + ":" + String(Int.random(in: 0...5)) + String(Int.random(in: 0...9)), imageName: song, isFavorite: false))
        }
        
        
        
        return songs
    }
}
