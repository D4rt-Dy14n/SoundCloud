//
//  SessionModel.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 23.07.2021.
//

import Foundation

struct SessionModel {
    let songModel: [SongModel]
}

struct SongModel {
    
    let artworkURL: String
    let duration: Int
    let id: Int
    let streamURL: String
    let title: String
    
    func timeString(time: TimeInterval) -> String {
        
        let sTime = Int(time) / 1000
        let minute = Int(sTime) / 60
        let second = Int(sTime) % 60
        
        return String(format: "%02i:%02i", minute, second)
    }
    
    var durationString: String {
        timeString(time: TimeInterval(duration))
    }
    
    var artist: String{
        if title.contains(" - ") {
            return title.components(separatedBy: " - ")[0]
        }
        else {
            return ""
        }
    }
    var songName: String {
        if title.contains(" - ") {
            return title.components(separatedBy: " - ")[1]
        } else {
            return title
        }
    }
}
