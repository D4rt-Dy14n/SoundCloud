//
//  SessionManager.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 23.07.2021.
//

import Foundation

protocol SessionManagerDelegate {
    func didUpdateSession(_ sessionManager: SessionManager, session: [SongModel])
    func didFailWithError(error:  Error)
}

struct SessionManager {
    
    let sessionURL = "https://api.soundcloud.com/tracks?"
    
    var delegate: SessionManagerDelegate?
    
    func fetchSession(searchText: String) {
        let urlString = sessionURL + "q=" + searchText + "&format=json" + "&client_id=" + SoundCloudClientID + "&limit=20&offset=0"
        performRequest(with: urlString)
    }
    
    func performRequest(with urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                
                if let safeData = data {
                    if let session = self.parseJSON(safeData) {
                        self.delegate?.didUpdateSession(self, session: session)
                        
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseJSON(_ sessionData: Data) -> [SongModel]? {
        let decoder = JSONDecoder()

        do {
            
            let decodedData = try decoder.decode([SongElement].self, from: sessionData)
            
            var session = [SongModel]()

            
            for collection in decodedData {
                
                let id = collection.id
                let duration = collection.duration
                let streamURL = collection.streamURL
                let title = collection.title
                let artworkURL = collection.artworkURL ?? ""
                                
                let song = SongModel(artworkURL: artworkURL, duration: duration, id: id, streamURL: streamURL, title: title)
                print(song.id)
                
                session.append(song)
            }
            
            return session
            
        } catch {
            self.delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
}
