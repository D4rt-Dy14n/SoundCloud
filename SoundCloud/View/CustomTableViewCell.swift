//
//  CustomTableViewCell.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 28.06.2021.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet weak var imageOfSong: UIImageView!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var isFavoriteButton: UIButton!
    
    //MARK: - Actions
    @IBAction func isFavoriteButton(_ sender: UIButton) {

    }
}
