//
//  MainViewController.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 27.06.2021.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    
    //MARK: - Properties
    lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var sessionManager = SessionManager()
    let customCell = CustomTableViewCell()
    // let songs = Song.getSongs()
    
    var songsArray: [CDSong] = []
    var songs = [SongModel]()
    //MARK: - Private Methods
    
    private func loadSongs() {
        
        let fetchRequest: NSFetchRequest<CDSong> = CDSong.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            
            songsArray = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    private func saveSongs() {
        do {
            try context.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        tableView.reloadData()
    }
    
    private func removeAllSongs() {
        
        let fetchRequest: NSFetchRequest<CDSong> = CDSong.fetchRequest()
        if let objects = try? context.fetch(fetchRequest) {
            for object in objects {
                context.delete(object)
            }
        }
    }
    
    //MARK: - Public Methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadSongs()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sessionManager.delegate = self
        textField.delegate = self
        //getDataFromFile()
        
        //removeSongs()
        
        saveSongs()
    }
    
    func getDataFromFile() {
        let fetchRequest: NSFetchRequest<CDSong> = CDSong.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "title != nil")
        
        var records = 0
        
        do {
            let count = try context.count(for: fetchRequest)
            records = count
            print("Data is here")
        } catch {
            print(error.localizedDescription)
        }
        
        guard records == 0 else { return }
        let pathToFile = Bundle.main.path(forResource: "data", ofType: "plist")
        let dataArray = NSArray(contentsOfFile: pathToFile!)!
        
        for dictionary in dataArray {
            let entity = NSEntityDescription.entity(forEntityName: "CDSong", in: context)
            let song = NSManagedObject(entity: entity!, insertInto: context) as! CDSong
            let songDictionary = dictionary as! NSDictionary
            
            song.artist = songDictionary["artist"] as? String
            song.imageName = songDictionary["imageName"] as? String
            song.isFavorite = songDictionary["isFavorite"] as! Bool
            song.length = songDictionary["length"] as? String
            song.title = songDictionary["title"] as? String
            
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        let song = songs[indexPath.row]
        cell.songNameLabel?.text = song.songName
        cell.songNameLabel?.numberOfLines = 0
        cell.artistLabel?.text = song.artist
        cell.lengthLabel?.text = song.durationString

        if let url = URL(string: song.artworkURL) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }

                DispatchQueue.main.async {
                    cell.imageOfSong?.image = UIImage(data: data) ?? UIImage(named: "icon Black")
                }
            }
            
            let btn = customCell.isFavoriteButton
            btn?.setTitle("♡", for: .normal)
            btn?.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)

            task.resume()
        }

    
        return cell
    }
    
    //MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //MARK: - Function playsong() TBD
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Actions
    
    @IBAction func isFavoriteButton(_ sender: UIButton, view: CustomTableViewCell) {
        
        let buttonPosition = sender.convert(sender.bounds.origin, to: tableView)
        
        if let indexPath = tableView.indexPathForRow(at: buttonPosition) {
            let rowIndex = indexPath.row
            print(rowIndex)
            songsArray[rowIndex].isFavorite = !songsArray[rowIndex].isFavorite
            
            switch sender.titleLabel?.text {
            case "♡":
                sender.setTitle("♥︎", for: .normal)
                sender.setTitleColor(#colorLiteral(red: 1, green: 0.3333333333, blue: 0, alpha: 1), for: .normal)
            case "♥︎":
                sender.setTitle("♡", for: .normal)
                sender.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            case .none:
                break
            case .some(_):
                break
            }
            saveSongs()
        }
    }
}

//MARK: - SessionManagerDelegate
extension MainViewController: SessionManagerDelegate {
    func didUpdateSession(_ sessionManager: SessionManager, session: [SongModel]) {
        DispatchQueue.main.async {

            self.songs = session
            self.tableView.reloadData()
        }
    }

    func didFailWithError(error: Error) {
        print(error)
        
    }
    
}

//MARK: - UITextFieldDelegate

extension MainViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {

            return true
        } else {
            textField.placeholder = "Search here"
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text {
            sessionManager.fetchSession(searchText: text)
            
            tableView.reloadData()
        }
        
        textField.text = ""
    }
}
