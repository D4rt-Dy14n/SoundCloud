//
//  FavoritesVC.swift
//  SoundCloud
//
//  Created by Юрий Федоров on 27.06.2021.
//

import UIKit
import CoreData

class FavoritesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: - Outlets
    @IBOutlet weak var favoriteTableView: UITableView!
    @IBOutlet weak var favoritesSearchBar: UISearchBar!
    
    //MARK: - Properties
    lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    var songsArray: [CDSong] = []

    //MARK: - Private Methods
    
    private func loadSongs() {
       
        let fetchRequest: NSFetchRequest<CDSong> = CDSong.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isFavorite = true")
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            
            songsArray = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    private func saveSongs() {
        do {
            try context.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        favoriteTableView.reloadData()
    }
    
    
    //MARK: - Public Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        loadSongs()
    }
    
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return songsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        let song = songsArray[indexPath.row]
        cell.songNameLabel?.text = song.title
        cell.songNameLabel?.numberOfLines = 0
        cell.artistLabel?.text = song.artist
        cell.lengthLabel?.text = song.length
        cell.imageOfSong?.image = UIImage(named: song.imageName ?? "icon Black")
        return cell
    }
    
    //MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//MARK: - Function playsong() TBD
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
//MARK: - Actions

        @IBAction func isFavoriteButton(_ sender: UIButton) {
            switch sender.titleLabel?.text {
            case "♡":
                sender.setTitle("♥︎", for: .normal)
                sender.setTitleColor(#colorLiteral(red: 1, green: 0.3333333333, blue: 0, alpha: 1), for: .normal)

                //MARK: - Function addToFavorites() TBD
                
                saveSongs()
            
            case "♥︎":
                sender.setTitle("♡", for: .normal)
                sender.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)

                saveSongs()

            //MARK: - Function deleteFromFavorites() TBD
            
            case .none:
                break
            case .some(_):
                break
            }
        }
}

//MARK: - Search Bar Methods
    extension FavoritesVC: UISearchBarDelegate {
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            
            let fetchRequest: NSFetchRequest<CDSong> = CDSong.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ OR artist CONTAINS[cd] %@", searchBar.text!)
            let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            do {
                
                songsArray = try context.fetch(fetchRequest)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
            if songsArray.count != 0 {
                
                favoriteTableView.reloadData()
            } else {
               self.loadSongs()
                searchBar.text = ""
                searchBar.placeholder = "No such comparison"
            }
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            if searchBar.text?.count == 0 {
                loadSongs()
                
                DispatchQueue.main.async {
                    searchBar.resignFirstResponder()
                }
            }
        }
    }
